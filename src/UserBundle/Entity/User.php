<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Validator\Constraints as MyAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  /**
  * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User", mappedBy="myFriends")
  */
  private $friendsWithMe;

  /**
  * @ORM\Column(name="site_web", type="string", length=255, nullable=true)
  * @Assert\Length(
  *     max=255,
  *     groups={"Registration", "Profile"}
  * )
  * @Assert\Url()
  */
  private $site_web = null;

  /**
  * @ORM\Column(name="telephone", type="string", length=10, nullable=true)
  *@MyAssert\ContainsTelephone
  */
  private $telephone = null;

  /**
  * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
  * @Assert\Length(
  *     min=3,
  *     max=255,
  *     minMessage="Le nom est trop court.",
  *     maxMessage="Le nom du site est trop long..",
  *     groups={"Registration", "Profile"}
  * )
  */
  private $adresse = null;

  /**
  *@ORM\ManyToMany(targetEntity="UserBundle\Entity\User", inversedBy="friendsWithMe", cascade={"persist"})
  *@ORM\JoinTable(name="friends",
  *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
  *      inverseJoinColumns={@ORM\JoinColumn(name="friend_user_id", referencedColumnName="id")}
  *      )
  */
  private $myFriends;


    /**
     * Set site_web
     *
     * @param string $siteWeb
     * @return User
     */
    public function setSiteWeb($siteWeb)
    {
        $this->site_web = $siteWeb;

        return $this;
    }

    /**
     * Get site_web
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->site_web;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Add friendsWithMe
     *
     * @param \UserBundle\Entity\User $friendsWithMe
     * @return User
     */
    public function addFriendsWithMe(\UserBundle\Entity\User $friendsWithMe)
    {
        $this->friendsWithMe[] = $friendsWithMe;

        return $this;
    }

    /**
     * Remove friendsWithMe
     *
     * @param \UserBundle\Entity\User $friendsWithMe
     */
    public function removeFriendsWithMe(\UserBundle\Entity\User $friendsWithMe)
    {
        $this->friendsWithMe->removeElement($friendsWithMe);
    }

    /**
     * Get friendsWithMe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriendsWithMe()
    {
        return $this->friendsWithMe;
    }

    /**
     * Add myFriends
     *
     * @param \UserBundle\Entity\User $myFriends
     * @return User
     */
    public function addMyFriend(\UserBundle\Entity\User $myFriends)
    {
        $this->myFriends[] = $myFriends;

        return $this;
    }

    /**
     * Remove myFriends
     *
     * @param \UserBundle\Entity\User $myFriends
     */
    public function removeMyFriend(\UserBundle\Entity\User $myFriends)
    {
        $this->myFriends->removeElement($myFriends);
    }

    /**
     * Get myFriends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyFriends()
    {
        return $this->myFriends;
    }
}
