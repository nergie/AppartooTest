<?php
namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      //Ajout de champ dans le formulaire de modification de fosuserBundle
        $builder
          ->add('telephone', 'text', array('required' => true))
          ->add('site_web', 'text', array('required' => false))
          ->add('adresse', 'text', array('required' => false));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }


    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
