<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller
{
    /**
    * @Route("/tous-les-contacts/{page}", name="all_contacts")
    */
    public function allContactsAction($page=1)
    {
      if ($page < 1) {
        $page = 1;
      }

      // récupèration de l'utilisateur connecté
      $user = $this->getUser();
      $nbPerPage = $this->container->getParameter('nb_contact_per_page');
      // Retourne la liste des contacts de l'application par ordre alphabétique de nom disponible à la page $page
      $listFriends = $this->getDoctrine()
      ->getManager()
      ->getRepository('UserBundle:User')
      ->getListContact($page, $nbPerPage)
      ;
      $nbPages = ceil(count($listFriends)/$nbPerPage);

      if ($page > $nbPages) {
        $page = 1;
      }

      return $this->render('UserBundle:Contacts:show_all_contacts.html.twig', array(
        'contacts' => $listFriends,
        'nbPages'  => $nbPages,
        'page'     => $page
      ));
    }

    public function listFriendsAction($max=5)
    {
      $nbPerSideBar = $this->container->getParameter('nb_contact_in_side_bar');
      // récupèration de l'utilisateur connecté
      $user = $this->getUser();
      $mesContacts = $this->getDoctrine()
      ->getManager()
      ->getRepository('UserBundle:User')
      ->getMyFriends($user->getId(),1, $nbPerSideBar)
      ;

      return $this->render('UserBundle:Contacts:listFriends.html.twig', array(
        'mesContacts' => $mesContacts
      ));
    }

    /**
    * @Route("/mes-contacts/{page}", name="mes_contacts")
    */
    public function mesContacts($page = 1)
    {
      if ($page < 1) {
        $page = 1;
      }
      // récupèration de l'utilisateur connecté
      $user = $this->getUser();
      $nbPerPage = $this->container->getParameter('nb_contact_per_page');
      // Retourne la liste des contacts enregistrée par l'utilisateur par ordre alphabétique de nom disponible à la page $page
      $mesContacts = $this->getDoctrine()
      ->getManager()
      ->getRepository('UserBundle:User')
      ->getMyFriends($user->getId(),$page, $nbPerPage)
      ;

      $nbPages = ceil(count($mesContacts)/$nbPerPage);

      if ($page > $nbPages) {
        $page = 1;
        $nbPages = 1;
      }

      return $this->render('UserBundle:Contacts:mesContacts.html.twig', array(
        'contacts' => $mesContacts,
        'nbPages'  => $nbPages,
        'page'     => $page
      ));
    }

    /**
    * @Route("/ajouter-contact/{id_friend}", name="ajouter_contact")
    */
    public function addFriendAction($id_friend, Request $request)
    {
      $userManager = $this->get('fos_user.user_manager');
      $friend = $userManager->findUserBy(array('id'=>$id_friend));
      $user = $this->getUser();
      if ( ($friend === null) || $friend === $user || $user->getMyFriends()->contains($friend)) {
          throw new NotFoundHttpException("Erreur lors de l'ajout du contact d'id ".$id_friend);
      }
      $user->addMyFriend($friend);
      $userManager->updateUser($user);
      $session = $request->getSession();
      $session->getFlashBag()->add('info', 'Contact '.$friend.' ajouté .');
      return $this->redirectToRoute('details_utilisateur', array('id'=>$id_friend));
    }


    /**
    * @Route("/details-utilisateur/{id}", name="details_utilisateur")
    */
    public function showUserAction($id)
    {
      $userManager = $this->get('fos_user.user_manager');
      $user = $userManager->findUserBy(array('id'=>$id));

      if($user === null){
        throw new NotFoundHttpException("L'utilisateur d'id ".$id." n'existe pas.");
      }

      return $this->render('UserBundle:Contacts:show_contact.html.twig', array(
        'user' => $user,
        //retourne vrai si l'utilisateur $id est déja dans le carnet d'adresse de l'utilisateur connecté
        'isAjouter'=>$user->getFriendsWithMe()->contains($this->getUser())
      ));
    }

    /**
    * @Route("/supprimer-contact/{id}", name="supprimer_contact")
    */
    public function deleteContactAction($id, Request $request)
    {
      $userManager = $this->get('fos_user.user_manager');
      $friend = $userManager->findUserBy(array('id'=>$id));
      $user = $this->getUser();
      if ( ($friend === null) || $friend === $user || !$user->getMyFriends()->contains($friend)) {
          throw new NotFoundHttpException("L'utilisateur d'id ".$id." n'est pas dans votre liste de contact.");
      }
      $user->removeMyFriend($friend);
      $userManager->updateUser($user);
      $session = $request->getSession();
      $session->getFlashBag()->add('info', 'Contact '.$friend.' supprimé .');
      return $this->redirectToRoute('homepage');
    }

}
