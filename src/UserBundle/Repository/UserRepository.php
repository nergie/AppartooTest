<?php

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class UserRepository extends EntityRepository
{
  //Récupère la liste de tous les utilisateurs de l'application triée par ordre alphabétique
  public function getListContact($page, $nbPerPage)
  {
    $query = $this
      ->createQueryBuilder('u')
      ->orderBy('u.username','ASC')
      ->getQuery()
      ->setFirstResult(($page-1) * $nbPerPage)
      ->setMaxResults($nbPerPage)
    ;

    return new Paginator($query, true);
  }

  //Récupère la liste des utilisateurs dans le carnet d'adresse de l'utilisateur $id_user triée par ordre alphabétique
  public function getMyFriends($id_user, $page, $nbPerPage)
  {
    $query = $this->getEntityManager()
      ->createQuery(
      'SELECT u FROM UserBundle:User u WHERE :friendsWithMe MEMBER OF u.friendsWithMe ORDER BY u.username ASC'
      )
      ->setParameter('friendsWithMe', $id_user)
      ->setFirstResult(($page-1) * $nbPerPage)
      ->setMaxResults($nbPerPage)
      ;

      return new Paginator($query, true);
  }

}
