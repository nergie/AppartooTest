<?php
namespace UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsTelephone extends Constraint
{
    public $message = 'Le numero "%string%" n\'est pas valide : il doit contenir 10 chiffres.';
}
